## Problem
Can you find the flag in this [file](https://www.easyctf.com/chals/autogen/62/hexedit)?

Problem Writer: r3ndom

## Flag
`b903b0f1`

Solved By: Ganden

### Solution
grep the file for `easyctf{` (don't need a hex editor)
