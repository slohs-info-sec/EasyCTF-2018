## Problem
Decode this `68657869745f6d6174655f3336353639363735333933303566626635323331`

Hint: It's encoded!

Problem Writer: soup

## Flag
`hexit_mate_3656967539305fbf5231`

Solved By: Ben

### Solution
Convert hex to string
