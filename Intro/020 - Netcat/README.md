## Problem
I've got a little flag for you! Connect to `c1.easyctf.com:12481` to get it, but you can't use your browser!

(Don't know how to connect? Look up TCP clients like Netcat. Hint: the Shell server has Netcat installed already!)

Here's your player key: `**********`. Several challenges might ask you for one, so you can get a unique flag!

Problem Writer: michael

## Flag
`hello_there!_AC24803ACFe4fEe7`

Solved By: Ganden

### Solution
```
$ nc c1.easyctf.com 12481
enter your player key: **********
thanks! here's your key: easyctf{hello_there!_AC24803ACFe4fEe7}
```
