## Problem
The web goes well beyond the surface of the browser! Warm up your web-sleuthing skills with this challenge by finding the hidden flag on [this page](https://www.easyctf.com/chals/autogen/92/index.html)!

Problem Writer: michael

## Flag
`hidden_from_the_masses_1afc0c`

Solved By: Ganden

### Solution
Inspect element or download the HTML file and open it in a text editor.
