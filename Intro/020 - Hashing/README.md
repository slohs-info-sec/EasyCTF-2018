## Problem
Cryptographic hashes are pretty cool! Take the SHA-512 hash of [this file](https://www.easyctf.com/chals/autogen/98/image.png), and submit it as your flag.

Hint: Try searching the web to find out what SHA-512 is. 

Problem Writer: gengkev

## Flag
`056a813578772859ebb03c3ff0213105201ee2eb2b9ea78177bf1b6d1ae5b5e55d292b49970ecb1f4780169843a11c3ef2d731c9fe4933b4cd772c2d2edda7af`

Solved By: Ganden

### Solution
You should be able to figure this one out.
