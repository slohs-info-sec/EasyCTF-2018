## Problem
This is literally one of oldest tricks in the book. To be precise, from the year AD 56.

Crack me. `bxpvzqc{t3iz0j3_70_345vz7c_c31209}`

Hint: Et tu, Brute?

Problem Writer: ztaylor54

## Flag
`w3lc0m3_70_345yc7f_f31209`

Solved By: Ganden

### Solution
ROT/Caesar cipher. Use a tool to find all possible rotations and match the string's prefix to `easyctf`.
