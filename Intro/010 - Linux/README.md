## Problem
Log into the shell server! You can do this in your browser by clicking on the Shell server link in the dropdown in the top right corner, or using an SSH client by following the directions on that page.

Once you've logged in, you'll be in your home directory. We've hidden something there! Try to find it. :)

Problem Writer: michael

## Flag
`i_know_how_2_find_hidden_files!`

Solved By: Ganden

### Solution
```
$ ls -a
.  ..  .bash_logout  .bashrc  .cache  .cloud-locale-test.skip  .flag  .profile
$ cat .flag
easyctf{i_know_how_2_find_hidden_files!}
```
