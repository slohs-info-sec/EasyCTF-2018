## Problem
Using your favorite language of choice, print Hello, world! to the output.
* For Python, consider the print function.
* For Java, consider System.out.println.
* For CXX, consider including stdio.h and using the printf function.


Problem Writer: michael

## Solution
You should be able to figure this one out.

Solved By: Ganden
