## Problem
over and over and over and over and over and ...

Given a number `N`, print the string "over [and over]" such that the string contains `N` "over"s. There should not be newlines in the string.

For example:
* For `N` = 1, print "over".
* For `N` = 5, print "over and over and over and over and over".

Problem Writer: michael

## Solution
```
#!/usr/bin/python3

N = input()
out = ""
for i in range(N - 1):
    out += "over and "
out += "over"
print(out)
```

Solved By: Ganden
