## Problem
OK, OK, you got Hello, world down, but can you greet specific people?

You'll be given the input of a certain name. Please greet that person using the same format. For example, if the given input is `Michael`, print `Hello, Michael!`.

Problem Writer: michael

## Solution
```
#!/usr/bin/python3

print("Hello, " + input() + "!")
```

Solved By: Ganden
