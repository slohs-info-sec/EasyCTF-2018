## Problem
Given two integers `a` and `b`, return `a` xor `b`. Remember, the xor operator is a bitwise operator that's usually represented by the `^` character.

For example, if your input was `5 7`, then you should print `2`.

Problem Writer: michael

## Solution
```
#!/usr/bin/python3

a,b = [int(x) for x in input().split()]
print(a^b)
```

Solved By: Ganden
