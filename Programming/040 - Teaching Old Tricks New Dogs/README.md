## Problem
You can decode a Caesar cipher, but can you write a program to decode a Caesar cipher?

Your program will be given 2 lines of input, and your program needs to output the original message.

* First line contains `N`, an integer representing how much the key was shifted by. `1 <= N <= 26`
* Second line contains the ciphertext, a string consisting of lowercase letters and spaces.

For example:
```
6
o rubk kgyeizl
```
You should print
```
i love easyctf
```

Problem Writer: michael

## Solution
```
#!/usr/bin/python3

N = int(input())
ciphertext = input()

print(''.join([((chr(ord(char) - N + 26 * ((-1 if (ord(char) - N > ord('Z')) else 1 if (ord(char) - N < ord('A')) else 0) if char.isupper() else (-1 if (ord(char) - N > ord('z')) else 1 if (ord(char) - N < ord('a')) else 0) if char.islower() else 0))) if char.isalpha() else char) for char in ciphertext]))
```

Solved By: Ganden

Why use a oneliner? Why not! In my defense, the writer of this problem sides with me on the issue:
![](michael.png)
