## Problem
Head over to the shell and see if you can find the flag at `/problems/markovs_bees/`!

Hint: Don't do this by hand! 

Problem Writer: ztaylor54

## Flag
`grepping_stale_memes_is_fun`

Solved By: Ganden

### Solution
```
$ grep -R "easyctf" Linux/050\ -\ Markov\'s\ Bees/markovs_bees/
Linux/050 - Markov's Bees/markovs_bees/c/e/i/bee913.txt:easyctf{grepping_stale_memes_is_fun}
```
