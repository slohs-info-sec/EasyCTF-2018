## Problem
What is the flag? [flag](https://cdn.easyctf.com/4680b45d33184b3e3ad99338907d1fe7dfec8ddd4b43ac71da69154ce9a6035c_flag.txt)

Hint: What is this file?

Problem Writer: soup

## Flag
`FLaaaGGGGGg`

Solved By: Ganden

### Solution
Look at the top of the file.

Rename the file to a .png and take a look.
