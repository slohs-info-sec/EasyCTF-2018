## Problem
I got a letter in my email the other day... It makes me feel sad, but maybe it'll make you glad. :( [file](https://cdn.easyctf.com/f91196f4d79a82fa40639752490f35838654a052cc74927dcb19f0b58224ad61_myletter.docx)

Problem Writer: neptunia

## Flag
`r3j3ct3d_4nd_d3jected`

Solved By: Ganden

### Solution
Know that docx files can be opened as zip files to explore their XML contents. Doing so here reveals a special image amongst the real contents.
