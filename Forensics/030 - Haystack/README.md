## Problem
There's a flag hidden in [this haystack](https://www.easyctf.com/chals/autogen/75/haystack.txt).

Problem Writer: sso999

## Flag
`QEMVcyvbmdLBJbAoWAltyNOGU`

Solved By: Ganden

### Solution
grep the file for `easyctf{`
