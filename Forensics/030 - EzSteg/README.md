## Problem
There appears to be a message beyond what you can see in [soupculents.jpg](https://cdn.easyctf.com/e9360fa30ce8226e08ccb4c270f95454788836cb9cba2f1922b0a3a8c7346b85_soupculents.jpg).

Hint: The description is a hint.

Problem Writer: soup

## Flag
`l00k_at_fil3_sigS`

Solved By: Ganden

### Solution
grep the file for `easyctf{`
